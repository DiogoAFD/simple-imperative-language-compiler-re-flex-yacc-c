void stinit(Stack* st){
	st->top = -1;
}

void push(Stack* st,int item){
	st->top++;
	st->s[st->top] = item;
}

int pop(Stack* st){
	int item;
	item = st->s[st->top];
	st->top--;
	return item;
}

