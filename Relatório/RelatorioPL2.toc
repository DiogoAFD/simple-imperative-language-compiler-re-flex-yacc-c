\contentsline {chapter}{\numberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Estrutura do Relat\IeC {\'o}rio}{3}{section.1.1}
\contentsline {chapter}{\numberline {2}An\IeC {\'a}lise e Especifica\IeC {\c c}\IeC {\~a}o do problema}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}Descri\IeC {\c c}\IeC {\~a}o formal do problema}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Especifica\IeC {\c c}\IeC {\~a}o dos requisitos}{4}{subsection.2.1.1}
\contentsline {section}{\numberline {2.2}Descri\IeC {\c c}\IeC {\~a}o da Linguagem}{5}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Defini\IeC {\c c}\IeC {\~o}es Gerais}{5}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Opera\IeC {\c c}\IeC {\~o}es e Express\IeC {\~o}es}{6}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Controlo de fluxo}{6}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}Hip\IeC {\'o}teses Base}{7}{section.2.3}
\contentsline {chapter}{\numberline {3}Desenho e concep\IeC {\c c}\IeC {\~a}o da solu\IeC {\c c}\IeC {\~a}o}{8}{chapter.3}
\contentsline {section}{\numberline {3.1}Arquitectura da Aplica\IeC {\c c}\IeC {\~a}o}{8}{section.3.1}
\contentsline {section}{\numberline {3.2}Analisador L\IeC {\'e}xico}{8}{section.3.2}
\contentsline {section}{\numberline {3.3}Gram\IeC {\'a}tica Independente do contexto - GIC}{11}{section.3.3}
\contentsline {section}{\numberline {3.4}Estruturas de Dados}{12}{section.3.4}
\contentsline {section}{\numberline {3.5}Detalhes da implementa\IeC {\c c}\IeC {\~a}o}{13}{section.3.5}
\contentsline {subsection}{\numberline {3.5.1}Erros de tipo e variaveis n\IeC {\~a}o declaradas}{13}{subsection.3.5.1}
\contentsline {subsection}{\numberline {3.5.2}Carregamento de endere\IeC {\c c}os de Arrays}{13}{subsection.3.5.2}
\contentsline {subsection}{\numberline {3.5.3}Etiquetas para controlo de fluxo}{14}{subsection.3.5.3}
\contentsline {chapter}{\numberline {4}Conclus\IeC {\~a}o}{15}{chapter.4}
\contentsline {chapter}{\numberline {A}Testes}{17}{appendix.A}
\contentsline {section}{\numberline {A.1}Teste 1}{17}{section.A.1}
\contentsline {subsection}{\numberline {A.1.1}teste1.lpis}{17}{subsection.A.1.1}
\contentsline {subsection}{\numberline {A.1.2}out1.vm}{17}{subsection.A.1.2}
\contentsline {section}{\numberline {A.2}Teste 2}{18}{section.A.2}
\contentsline {subsection}{\numberline {A.2.1}teste2.lpis}{18}{subsection.A.2.1}
\contentsline {subsection}{\numberline {A.2.2}out2.vm}{19}{subsection.A.2.2}
\contentsline {section}{\numberline {A.3}Teste 3}{20}{section.A.3}
\contentsline {subsection}{\numberline {A.3.1}teste3.lpis}{20}{subsection.A.3.1}
\contentsline {subsection}{\numberline {A.3.2}out3.vm}{20}{subsection.A.3.2}
\contentsline {section}{\numberline {A.4}Teste 4}{22}{section.A.4}
\contentsline {subsection}{\numberline {A.4.1}teste4.lpis}{22}{subsection.A.4.1}
\contentsline {subsection}{\numberline {A.4.2}out4.vm}{23}{subsection.A.4.2}
\contentsline {chapter}{\numberline {B}Analisador L\IeC {\'e}xico}{27}{appendix.B}
\contentsline {chapter}{\numberline {C}Analisador Sint\IeC {\'a}ctico e Sem\IeC {\^a}ntico}{29}{appendix.C}
\contentsline {chapter}{\numberline {D}C\IeC {\'o}digo C}{34}{appendix.D}
\contentsline {chapter}{\numberline {E}Stack}{41}{appendix.E}
\contentsline {chapter}{\numberline {F}Makefile}{42}{appendix.F}
