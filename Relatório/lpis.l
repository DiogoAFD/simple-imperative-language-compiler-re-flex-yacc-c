%{
%}

%option yylineno

NUM 	[0-9]
ID 		[a-z][a-z0-9A-Z_]*

%%

if 							{return IF;}
else 						{return ELSE;}
do 							{return DO;}
for							{return FOR;}
while						{return WHILE;}
int							{return INT;}

!							{return not;}
[+-]						{yylval.vals = strdup(yytext);return opa;}
[*\/%]						{yylval.vals = strdup(yytext);return opm;}
[&|]						{yylval.vals = strdup(yytext);return opl;}
[<>]|">="|"<="|"=="|"!="	{yylval.vals = strdup(yytext);return opr;}
"++"						{return inc;}

printInt					{return PRINT_INT;}
print 						{return PRINT;}
scan						{return SCAN;}
{ID}						{yylval.vals = strdup(yytext);return id;}
\"[^\"]+\"					{yylval.vals = strdup(yytext);return str;}

{NUM}+						{yylval.vals = strdup(yytext);return num;}
[,;\{\}\[\]\(\)=]			{return yytext[0];}
\/\/.*						{;}
[ \t\n]+					{;}
.							{yyerror("Syntax error: Caracter desconhecido");}

%%

int yywrap(){
	return 1;
}