void initTabela(){
    int i;

    hcreate(MAX_ID*2);
    memPtr = 0;
    cntId = 0;

    for(i=0;i<MAX_ID;i++){
    	data[i].nome = NULL;
    	data[i].posBaseMem = -1;
    	data[i].tamanho = -1;
    	data[i].tipo = -1;
    }
}

void init(){
	erro = 0;	

	cntCiclo = 0;
	stinit(&stCiclo);
	cntCond = 0;
	stinit(&stCond);

	initTabela();

	if((fp = fopen("out.vm","w")) == NULL){
		erro = 1;
		printf("Erro ao abrir o ficheiro 'out.vm'\n");
	}
}

void genCode0(char* s){
    fprintf(fp,"%s\n",s);
}

void genCode1(char* s1,char* s2){
    fprintf(fp,"%s %s\n",s1,s2);
}

void getOp(char* c){
	if(!strcmp(c,"+"))			fprintf(fp, "add\n");
	else if(!strcmp(c,"-"))		fprintf(fp, "sub\n");
	else if(!strcmp(c,"*"))		fprintf(fp, "mul\n");
	else if(!strcmp(c,"/"))		fprintf(fp, "div\n");
	else if(!strcmp(c,"%"))		fprintf(fp, "mod\n");
	else if(!strcmp(c,"&"))		fprintf(fp, "mul\n");
	else if(!strcmp(c,"|"))		fprintf(fp, "add\n");
	else if(!strcmp(c,"<="))	fprintf(fp, "infeq\n");
	else if(!strcmp(c,">="))	fprintf(fp, "supeq\n");
	else if(!strcmp(c,">"))		fprintf(fp, "sup\n");
	else if(!strcmp(c,"<"))		fprintf(fp, "inf\n");
	else if(!strcmp(c,"=="))	fprintf(fp, "equal\n");
	else if(!strcmp(c,"!="))	fprintf(fp, "equal\nnot\n");
}

void insereId(char* ident, int tam, char tipo){
	
	ENTRY e, *ep;

	data[cntId].nome = ident;
	data[cntId].posBaseMem = memPtr;
	data[cntId].tamanho = tam;
	data[cntId].tipo = tipo;

	e.key = ident;
	e.data = (void*) (&data[cntId]);
	ep = hsearch(e,ENTER);

	if(ep == NULL){
		printf("Erro ao inserir ident\n");
		erro = 1;
	}else{

		memPtr+=tam;
		cntId = (cntId + 1) % MAX_ID;

		if(tipo==1){ // var atómica
			fprintf(fp, "pushi 0\n");
		}else{ // array
			fprintf(fp, "pushn %d\n", tam);
		}
	}
}

int existeId(char* ident, char tipo){
	Ident* aux;
	ENTRY e,*ep;
	int r = 0;

	e.key = ident;
	ep = hsearch(e,FIND);

	if(ep!=NULL){
		r = 1;
		aux = (Ident*)(ep->data);
		if(aux->tipo != tipo){
			printf("Erro de tipo na var %s\n",ident);
			erro = 1;
		}
	}else{
		r = 0;
		printf("Var %s não declarada\n",ident);
		erro = 1;
	}
	return r;
}

void incVar(char* ident){
	Ident* aux;
	ENTRY e,*ep;
	char buf[50];

	e.key= ident;
	ep = hsearch(e,FIND);

	if(ep != NULL){
		aux = (Ident*)(ep->data);
		sprintf(buf,"pushi 1\npushg %d\nadd\nstoreg %d\n",aux->posBaseMem,aux->posBaseMem);
		genCode0(buf);
	}else{
		printf("Erro a ler var\n");
		erro = 1;
	}
}

void pushId(char* ident){
	Ident* aux;
	ENTRY e,*ep;
	char buf[50];

	e.key = ident;
	ep = hsearch(e,FIND);

	if(ep!=NULL){
		aux = (Ident*)(ep->data);
		sprintf(buf,"pushg %d", aux->posBaseMem);
		genCode0(buf);
	}else{
		printf("Erro a ler var\n");
		erro = 1;
	}
}

void storeId(char* ident){
	Ident* aux;
	ENTRY e,*ep;
	char buf[50];

	e.key= ident;
	ep = hsearch(e,FIND);

	if(ep != NULL){
		aux = (Ident*)(ep->data);
		sprintf(buf,"storeg %d",aux->posBaseMem);
		genCode0(buf);
	}else{
		printf("Erro a ler var\n");
		erro = 1;
	}
}

void readId(char* ident){
	Ident* aux;
	ENTRY e,*ep;
	char buf[50];

	e.key = ident;
	ep = hsearch(e,FIND);

	if(ep!=NULL){
		aux = (Ident*)(ep->data);
		genCode0("read");
		genCode0("atoi");
		sprintf(buf,"storeg %d",aux->posBaseMem);
		genCode0(buf);
	}else{
		printf("Erro a ler var\n");
		erro = 1;
	}
}

void loadArrayAdd(char* ident){
	Ident* aux;
	ENTRY e,*ep;
	char buf[50];

	e.key = ident;
	ep = hsearch(e,FIND);

	if(ep!=NULL){
		aux = (Ident*)(ep->data);
		genCode0("pushgp");
		sprintf(buf,"pushi %d",aux->posBaseMem);
		genCode0(buf);
		genCode0("padd");
	}else{
		printf("Erro a ler var\n");
		erro = 1;
	}
}

void loadMatrixAdd(char* ident){
	Ident* aux;
	ENTRY e,*ep;
	char buf[50];

	e.key = ident;
	ep = hsearch(e,FIND);

	if(ep!=NULL){
		aux = (Ident*)(ep->data);
		genCode0("pushgp");
		sprintf(buf,"pushi %d",aux->posBaseMem);
		genCode0(buf);
		genCode0("mul");
		genCode0("add");
		genCode0("add");
	}else{
		printf("Erro a ler var\n");
		erro = 1;
	}
}

void fimFor(){
	char buf[50];
	int n = pop(&stCiclo);
	sprintf(buf,"jump loop%dA\nendloop%d:\n",n,n);
	genCode0(buf);
}

void forExpLog(){
	char buf[50];
	sprintf(buf,"jz endloop%d\njump loop%dB\nloop%dA: nop\n",cntCiclo,cntCiclo,cntCiclo);
	genCode0(buf);
}

void whileExpLog(){
	char buf[50];
	sprintf(buf,"jz endloop%d\n",cntCiclo);
	genCode0(buf);
}

void fimWhile(){
	char buf[50];
	int n = pop(&stCiclo);
	sprintf(buf,"jump loop%d\nendloop%d:\n",n,n);
	genCode0(buf);
}

void forIt(){
	char buf[50];
	sprintf(buf,"jump loop%d\nloop%dB: nop\n",cntCiclo,cntCiclo);
	genCode0(buf);
}

void iniCiclo(){
	char buf[50];
	cntCiclo++;
	push(&stCiclo,cntCiclo);
	sprintf(buf,"loop%d: nop",cntCiclo);
	genCode0(buf);
}

void fimCiclo(){
	char buf[50];
	int n = pop(&stCiclo);
	sprintf(buf,"jz endloop%d\njump loop%d\nendloop%d:\n",n,n,n);
	genCode0(buf);
}

void iniCond(){
	char buf[50];
	cntCond++;
	push(&stCond,cntCond);
	sprintf(buf,"jz else%d",cntCond);
	genCode0(buf);
}

void corpoCond(){
	char buf[50];
	int n = pop(&stCond);
	sprintf(buf,"jump endif%d",n);
	genCode0(buf);
	sprintf(buf,"else%d:",n);
	genCode0(buf);
	push(&stCond,n);
}

void fimCond(){
	char buf[50];
	int n = pop(&stCond);
	sprintf(buf,"endif%d:",n);
	genCode0(buf);
}