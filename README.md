# ** Simple Imperative Language Compiler** #

###  ###
For the development of this project was used the lexical analyzer Flex and Yacc parser generator.

It was developed a grammar for a Simple Imperative Language Compiler, called SILC, followed by a lexical, semantic and syntactic recognition. The language allows only the basic functions of imperative programming. You can only handle integer or integer array type variables, with one or two positions. These same variables must be declared globally, at the start of the code and can not exist redeclarations of the same.

The compiler for this language should generate pseudocode for the Assembly Virtual Machine provided.