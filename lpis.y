%{

#include <stdio.h>
#include <string.h>
#include <search.h>


int erro;
FILE* fp;
int cntCiclo;
int cntCond;
int memPtr;
int cntId;

#define SIZE 100
typedef struct stack{
	int s[SIZE];
	int top;
}Stack;

Stack stCiclo;
Stack stCond;

#include "stack.c"

typedef struct ident{
	char* nome;
	int posBaseMem;
	int tamanho;
	char tipo;
}Ident;

#define MAX_ID 500
Ident data[MAX_ID];

#include "codigoC.c"

%}

%union{
	int vali;
	char* vals;
}

%start Programa
%token <vals>id
%token <vals>str
%token <vals>num
%token INT WHILE FOR IF ELSE PRINT PRINT_INT SCAN DO not inc
%token <vals>opa
%token <vals>opl
%token <vals>opm
%token <vals>opr

%left opa
%left opm
%left opl
%nonassoc opr

%%

Programa	: ListaVars 										{genCode0("start");} 
			  ListaInst 										{genCode0("stop");}
			;

ListaVars	:
			| ListaVars DeclVar
			| DeclVar
			;

DeclVar		: Tipo id ';'										{insereId($2,1,1);}
			| Tipo id '[' num ']' ';'							{insereId($2,atoi($4),2);}
			;

Tipo		: INT
			;

ListaInst	: ListaInst Inst
			| Inst
			;

Inst 		: If
			| While
			| DoWhile
			| For
			| Atrib ';'
			| Io
			;

Atrib		: id '=' Exp 										{if(existeId($1,1)) storeId($1);}
			| id inc											{incVar($1);}
			| id 												{if(existeId($1,2)) loadArrayAdd($1);}
			  '[' Exp ']' '=' Exp 								{genCode0("storen");}
			;

Io			: Print 						
			| Scan 
			;

Print 		: PRINT_INT '(' Exp ')' ';'							{genCode0("writei");}
			| PRINT '(' str ')' ';'								{genCode1("pushs",$3);genCode0("writes");}
			;

Scan 		: SCAN '(' id ')' ';'								{if(existeId($3,1)) readId($3);}
			;

While 		: WHILE 											{iniCiclo();}
			  '(' ExpLog ')'									{whileExpLog();}
			  '{' ListaInst '}'									{fimWhile();}
			;

DoWhile		: DO 												{iniCiclo();}
			  '{' ListaInst '}' WHILE '(' ExpLog ')' ';'		{fimCiclo();}
			;

For 		: FOR ForHeader '{' ListaInst '}'					{fimFor();}
			;

ForHeader 	: '(' ForAtrib ';' 									{iniCiclo();}
			  ExpLog ';'										{forExpLog();}
			  ForAtrib ')'										{forIt();}
			;

ForAtrib 	: Atrib
			|
			;

If			: IF '(' ExpLog ')'									{iniCond();} 
			  '{' ListaInst '}' 								{corpoCond();}
			  Else 												{fimCond();}
			;

Else		: 
			| ELSE '{' ListaInst '}'
			;

Exp 		: Termo
			| Exp opa Termo										{getOp($2);}
			;

Termo 		: Fator
			| Termo opm Fator									{getOp($2);}
			;

Fator 		: num												{genCode1("pushi",$1);}
			| id 												{if(existeId($1,1)) pushId($1);}
			| id 												{if(existeId($1,2)) loadArrayAdd($1);}
			  '[' Exp ']'										{genCode0("loadn");}
			| '(' Exp ')'
			;

ExpCond	 	: Exp opr Exp 										{getOp($2);}
			| Exp
			| '(' ExpCond ')'
			;

ExpLog 		: ExpLog opl ExpLog 								{getOp($2);} 								
			| ExpCond
			| '(' ExpLog ')'
			| not ExpLog 										{genCode0("not");}
			;

%%

#include "lex.yy.c"

int yyerror(char *s){
	erro = 1;
    fprintf(stderr,"%s at line %d in '%s'\n",s,yylineno,yytext);
    return 0;
}

int main(int argc,char* argv[]){
	extern FILE* yyin;
	yyin = fopen(argv[1],"r");

	init();
	yyparse();
	hdestroy();

	if(erro){
		fclose(fp);
		fp = fopen("out.vm","w");
	}

	fclose(fp);

	return 0;
}