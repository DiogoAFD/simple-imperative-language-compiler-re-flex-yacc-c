CFLAGS=-O2

all: 
	flex lpis.l
	yacc -d -v lpis.y
	gcc $(CFLAGS) y.tab.c -o exec

clean: 
	rm y.output y.tab.c y.tab.h lex.yy.c